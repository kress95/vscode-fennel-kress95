# vscode-fennel-kress95

Fennel syntax support for vscode.

Based on [shenlebantongying's vscode-lisp-syntax](https://github.com/shenlebantongying/vscode-lisp-syntax).

Very WIP (and probably slow), but colorizes better than what is currently available.
